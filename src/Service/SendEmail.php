<?php


namespace App\Service;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SendEmail
{
    private MailerInterface $mailer;
    private string $senderEmail;
    //private  $senderName;

    /**
     * SendEmail constructor.
     * @param MailerInterface $mailer
     * @param $senderEmail
     */
    public function __construct(MailerInterface $mailer, string $senderEmail)
    {
        $this->mailer = $mailer;
        $this->senderEmail = $senderEmail;
    }

    /**
     * SendEmail constructor.
     * @param MailerInterface $mailer
     * @param  $senderEmail
     *
     */


    /**
     * @param array<mixed> $arguments
     * @throws TransportExceptionInterface
     */
    public function send(array $arguments):void{
        [
            'recipient_email' => $recipientEmail,
            'subject'         => $subject,
            'html_template'   => $htmlTemplate,
            //'context'         => $context,
        ]= $arguments;
        $email = new Email();
        $email->from(new Address($this->senderEmail))
                ->to($recipientEmail)
                ->subject($subject)
                ->html($htmlTemplate);
            $this->mailer->send($email);

    }

}