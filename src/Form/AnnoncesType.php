<?php

namespace App\Form;

use App\Entity\Annonces;
use App\Entity\Departements;
use App\Entity\OuvertA;
use App\Entity\Region;
use App\Entity\Villes;
use App\Repository\DepartementsRepository;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;

class AnnoncesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,[
                'label'=> 'Titre',
                'attr'=>[
                    'placeholder'=>'Choisir un petit titre votre annonce'
                ]
            ]);
        $builder->add('typeConcours');

        $builder->add('content', TextType::class,[
                'label'=>'Contenu de l\'annonce',
                'required' =>true,
                'attr'=>[
                    'placeholder'=>'Le contenu de votre annonce !'
                ]
            ]);

            $builder->add('image', FileType::class, [
                'data_class' => null,
                'label' =>'Photo de l\'affiche',
                'required' =>false,
                'attr'=>[
                    'placeholder'=>'Selectionner la photo principale !',
                ]
            ]);
        $builder->add('description', TextType::class, [
                'mapped' => false,
                'required' =>true,
                'attr'=>[
                    'placeholder'=>'Description de votre publication',
                ]
            ]);
        $builder->add('adImages', FileType::class, [

                'multiple' => true,
                'mapped' => false,
                'required' =>false,
                'data_class' => null,
                'label'=> 'Photos pour le slide',
                'attr'=>[
                    'placeholder'=>'Selectionner 1 ou 2 photos de slid !',
                ]

            ]);
            $builder->add('region', EntityType::class,[
                'class' => Region::class,
                'attr' =>['placeholder'=> 'Selectionner votre region'],
                'mapped'=>false,
                'required'=>false
            ]);
        $builder->add('createdAt',DateTimeType::class, [
            'label'=>"Date de l'événement",
            'data'=> new DateTime(),//date actuelle
    ]);
        $builder->add('ouvertA', EntityType::class,[
            'class' => OuvertA::class,
            'label'=>"Concours ouvert à :",

        ]);

        $builder->get('region')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->addDepartementField($form->getParent(), $form->getData());
            }
        );
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $data = $event->getData();
                /* @var $ville Villes */
                $ville = $data->getVille();
                $form = $event->getForm();
                if ($ville) {
                    $departement = $ville->getDepartements();
                    $region = $departement->getRegions();
                    $this->addDepartementField($form, $region);
                    $this->addVilleField($form, $departement);
                    $form->get('region')->setData($region);
                    $form->get('departement')->setData($departement);
                } else {
                    $this->addDepartementField($form, null);
                    $this->addVilleField($form, null);
                }
            }
        );



    }

    /**
     * Rajoute un champs departement au formulaire
     * @param FormInterface $form
     * @param Region|null $region
     */
    private function addDepartementField(FormInterface $form, ?Region $region)
    {
        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'departement',
            EntityType::class,
            null,
            [
                'class'           => Departements::class,
                'placeholder'     => $region ? 'Sélectionnez votre département' : 'Sélectionnez votre région',
                'mapped'          => false,
                'required'        => false,
                'auto_initialize' => false,
                'choices'         => $region ? $region->getDepartements() : []
            ]
        );
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->addVilleField($form->getParent(), $form->getData());
            }
        );
        $form->add($builder->getForm());
    }


    /**
     * Rajoute un champs ville au formulaire
     * @param FormInterface $form
     * @param Departements|null $departement
     */
    private function addVilleField(FormInterface $form, ?Departements $departement)
    {
        $form->add('ville', EntityType::class, [
            'class'       => Villes::class,
            'placeholder' => $departement ? 'Sélectionnez votre ville' : 'Sélectionnez votre département',
            'choices'     => $departement ? $departement->getVilles() : []
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonces::class,

        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'annonces';
    }
}
