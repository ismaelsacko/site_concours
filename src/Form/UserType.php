<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',TextType::class,[
                'label' => 'Votre prénom',
                'attr' => [
                    'placeholder' =>'Entrer votre prénom'
                ],
            ])
            ->add('lastName',TextType::class,[
                'label' => 'Votre nom',
                'attr' => [
                    'placeholder' =>'Entrer votre nom'
                ],
            ])
            ->add('email',EmailType::class,[
                'label' => 'Votre email',
                'attr' => [
                    'placeholder' =>'Entrer votre email'
                ],
            ])
            ->add('hash', PasswordType::class,[
                'label' => 'Votre mot de passe',
                'attr'=>[
                    'placeholder' =>'Tapez votre mot de passe'
                ]

            ])
            ->add('confirmPassword', PasswordType::class,[
                'label' =>'Confirmer le mot de passe',
                'attr'=>[
                    'placeholder' =>'Confirmez votre mot de passe'
                ]
            ])
            ->add('picture', FileType::class,[
                'mapped' =>false,
                    'label' => 'Votre photo de profile',
                    'attr' => [
                        'placeholder' =>'Télécharger une image, Champ obligatoire !!!'
                    ],

                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
