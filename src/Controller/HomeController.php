<?php

namespace App\Controller;

use App\Repository\AnnoncesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param AnnoncesRepository $annoncesRepository
     * @return Response
     */
    public function index(AnnoncesRepository $annoncesRepository)
    {
        $content = "Binvenue sur notre site d'annonces des concours !";
        return $this->render('home/index.html.twig', [
            'content' =>$content,
            'ads' => $annoncesRepository->findAll(),

        ]);
    }

    /**
 * @Route("/search", name="search_annonces", methods={"GET"})
 */
public function search(Request $request, AnnoncesRepository $annoncesRepository): JsonResponse
{
    // Récupération du terme de recherche depuis l'URL (ex: /search?query=ma+recherche)
    $query = $request->query->get('query');

    // On utilise une méthode du repository (voir la suite) pour chercher dans le titre, le contenu ou le nom de la ville
    $annonces = $annoncesRepository->searchByQuery($query);

    // Construction du tableau de résultats
    $results = [];
    // Exemple dans HomeController.php
    foreach ($annonces as $annonce) {
        $results[] = [
            'id'      => $annonce->getId(),
            'title'   => $annonce->getTitle(),
            'content' => substr($annonce->getContent(), 0, 100),
            'ville'   => $annonce->getVille() ? $annonce->getVille()->getName() : '',
            'slug'    => $annonce->getSlug(),
            'image'   => $annonce->getImage(), // Ajout de la propriété image
            'date'    => $annonce->getCreatedAt()->format('d/m/Y'),
            'author'  => $annonce->getAuthor()->getUsername(),
            'departement' => $annonce->getVille()->getDepartements()->getName(),
            'region' => $annonce->getVille()->getDepartements()->getRegions()->getName()
        ];
    }


    return new JsonResponse(['annonces' => $results]);
}



}
