<?php

namespace App\Controller;

use App\Entity\Annonces;
use App\Entity\Image;
use App\Entity\User;
use App\Form\AnnoncesType;
use App\Repository\AnnoncesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;



/**
 * @Route("/annonce")
 */
class AnnoncesController extends AbstractController
{
    /**
     * @Route("/", name="annonces_index", methods={"GET"})
     * @param AnnoncesRepository $annoncesRepository
     * @return Response
     */
    public function index(AnnoncesRepository $annoncesRepository): Response
    {
        return $this->render('annonces/index.html.twig', [
            'ads' => $annoncesRepository->findAll(),

        ]);
    }


    /**
     * @Route("/new", name="annonces_new", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param MailerInterface $mailer
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function new(Request $request, MailerInterface $mailer): Response
    {
        $ads = new Annonces();
        $form = $this->createForm(AnnoncesType::class, $ads);
        $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {

            //REception et traimment de l'image de couverture de notre annonce
            $coverImage = $form->get('image')->getData();
            $myFile =md5(uniqid()).'.' .$coverImage->guessExtension();

            $coverImage->move(
                $this->getParameter('images_directory'), $myFile);
            $ads->setImage($myFile);


           //on récupère les images uploadées
           $images = $form->get('adImages', 'description')->getData();
           //On boucle sur les images
           foreach ($images as $image) {
               //on génère un nouveau nom de fichier
               //ici on chosi un nom aléatoire pour les + l'extention de celui-ci
               $fichier = md5(uniqid()) . '.' . $image->guessExtension();
               //On copie les fichier dans le dossier upload
               $image->move(
                   $this->getParameter('images_directory'), $fichier
               );
               //On stocke le nom de l'image dans la bdd
               $img = new Image();
               $img->setUrl($fichier);
               $ima = $form->get('description')->getData();
               $img->setDescription($ima);
               $ads->addImage($img);

           }
           $ads->setAuthor($this->getUser());
           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($ads);
           $entityManager->flush();

           $email = new Email();
           $users = $this->getDoctrine()->getRepository(User::class);
           $userEmail = $users->findAll();
           foreach($userEmail as $value){
               $email->from(new Address('ismalsacko@gmail.com'))
                   ->to($value->getEmail())
                   ->subject('Nouvelle notification ('.$ads->getTitle().')')
                   ->embedFromPath('uploads/'.$ads->getImage(),'Image_annonce')
                   ->html('Vous avez une nouvelle notification de concours, publié par '.$ads->getAuthor()
                           ->getFullName(). ', <a href="http://www.ismaeldev.fr/annonce/">
                            Pour aller sur site</a>,<img width="500" src="cid:Image_annonce">')
                   ->from(Address::fromString('Ismael SACKO <ismalsacko@gmail.com>'));
               //$email->getHeaders()->addTextHeader('X-Transport', 'alternative');

               $mailer->send($email);
           }
            return $this->redirectToRoute('annonces_index', [
                'slug' =>$ads->getAuthor()
            ]);

        }

        return $this->render('annonces/new.html.twig', [
            'ads' => $ads,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="annonces_show", methods={"GET"})
     * @param Annonces $annonce
     * @return Response
     */
    public function show(Annonces $annonce): Response
    {
        return $this->render('annonces/show.html.twig', [
            'user' =>$this->getUser(),
            'ads' => $annonce,
        ]);
    }

    /**
     * @Route ("/notify/{slug}", name="notify_index", methods={"GET"})
     * @param Annonces $annonce
     * @return Response
     */
    function notification(Annonces $annonce){


        return $this->render('annonces/notify.html.twig',[
            'ads'=>$annonce,
        ]);

    }

    /**
     * @Route("/{id}/edit", name="annonces_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_USER') and user === annonce.getAuthor() ",
        * message="Cette annonce ne vous appartient pas, vous ne pouvez pas la modifier")
     * @param Request $request
     * @param Annonces $annonce
     * @return Response
     * @param Request $request
     * @param Annonces $annonce
     * @return Response
     * @param Request $request
     * @param Annonces $annonce
     * @return Response
     */
    public function edit(Request $request, Annonces $annonce): Response
    {
        $form = $this->createForm(AnnoncesType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //REception et traimment de l'image de couverture de notre annonce
            $coverImage = $form->get('image')->getData();
            $myFile = md5(uniqid()).'.' .$coverImage->guessExtension();
            $coverImage->move(
                $this->getParameter('images_directory'), $myFile);
            $annonce->setImage($myFile);
            //on récupère les images uploadées
            $images = $form->get('adImages')->getData();
            //On boucle sur les images
            foreach ($images as $image) {
            //unlink($this->getParameter('images_directory').'/'.$annonce->getImage());
                //on génère un nouveau nom de fichier
                //ici on chosi un nom aléatoire pour les + l'extention de celui-ci
                $fichier =  md5(uniqid()).'.' . $image->guessExtension();
                //unlink($this->getParameter('images_directory').'/'.$fichier);
                //On copie les fichier dans le dossier upload
                $image->move(
                    $this->getParameter('images_directory'), $fichier
                );
                //On stocke le nom de l'image dans la bdd
                $img = new Image();
                $img->setUrl($fichier);
                $ima = $form->get('description')->getData();
                $img->setDescription($ima);
                $annonce->addImage($img);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('annonces_index');
        }

        return $this->render('annonces/edit.html.twig', [
            'ads' => $annonce,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="annonces_delete", methods={"DELETE"})
     * @param Request $request
     * @param Annonces $annonce
     * @return Response
     */
    public function delete(Request $request, Annonces $annonce): Response
    {
        if ($this->isCsrfTokenValid('delete' . $annonce->getId(), $request->request->get('_token'))) {
            $imgdelete = $annonce->getImage();
            if ($imgdelete) {
                $file = $this->getParameter('images_directory') . '/' . $imgdelete;
                if (file_exists($file)) {
                    unlink($file); // On supprime le fichier s'il existe
                } else {
                    $this->addFlash('danger', 'Fichier non trouvé'); // On envoie un message d'erreur
                }
    
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($annonce);
                $entityManager->flush();
            }
        }
    
        return $this->redirectToRoute('annonces_index');
    }
    

}
