<?php

namespace App\Repository;

use App\Entity\OuvertA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OuvertA|null find($id, $lockMode = null, $lockVersion = null)
 * @method OuvertA|null findOneBy(array $criteria, array $orderBy = null)
 * @method OuvertA[]    findAll()
 * @method OuvertA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OuvertARepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OuvertA::class);
    }

    // /**
    //  * @return OuvertA[] Returns an array of OuvertA objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OuvertA
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
