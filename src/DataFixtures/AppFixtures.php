<?php

namespace App\DataFixtures;

use App\Entity\Departements;
use App\Entity\Region;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\Villes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use ParseCsv\Csv;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encorder;


    /**
     * AppFixtures constructor.
     * @param $encorder
     */
    public function __construct( UserPasswordEncoderInterface $encorder)
    {
        $this->encorder = $encorder;


    }

    public function load(ObjectManager $manager)
    {
        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $manager->persist($adminRole);
        $suer1 = new User();
        $password = 'sackosacko';
        $suer1->setFirstName('SACKO')
             ->setLastName('Ismaila')
             ->setEmail('ismalsacko@yahoo.fr')
             ->setPicture('967c6b175e77492c339e7afc4102598f')
            ->setHash($this->encorder->encodePassword($suer1,$password))
            ->addUsersRole($adminRole);

        $manager->persist($suer1);

        $suer2 = new User();
        $suer2->setFirstName('ADMIN')
            ->setLastName('Admin')
            ->setEmail('admin_amu@yahoo.fr')
            ->setPicture('967c6b175e77492c339e7afc4102598f')
            ->setHash($this->encorder->encodePassword($suer2,'admin'))
            ->addUsersRole($adminRole);

        $manager->persist($suer2);


        /*$csv = dirname($this->getContainer->get('kernel')->getRootDir()) . DIRECTORY_SEPARATOR . 'var' .
            DIRECTORY_SEPARATOR . 'villes.csv';*/
        $csv_2= file('var/villes.csv');
        $lines = $csv_2;
        $regions = [];
        $departements = [];
        $villes = [];

        foreach ($lines as $k => $line) {
            $line = explode(';', $line);
            if (count($line) > 10 && $k > 0) {
                // On sauvegarde la region
                if (!key_exists($line[1], $regions)) {
                    $region = new Region();
                    $region->setCode($line[1]);
                    $region->setName($line[2]);
                    $regions[$line[1]] = $region;
                    $manager->persist($region);
                } else {
                    $region = $regions[$line[1]];
                }

                // On sauvegarde le departement
                if (!key_exists($line[4], $departements)) {
                    $departement = new Departements();
                    $departement->setName($line[5]);
                    $departement->setCode(intval($line[4]));
                    $departement->setRegions($region);
                    $departements[$line[4]] = $departement;
                    $manager->persist($departement);
                } else {
                    $departement = $departements[$line[4]];
                }

                // On sauvegarde la ville
                $ville = new Villes();
                $ville->setName($line[8]);
                $ville->setCode(intval($line[9]));
                $ville->setDepartements($departement);
                $villes[] = $line[8];
                $manager->persist($ville);
            }
        }

        $manager->flush();


    }
}
