// $(document).on('change', '#annonces_region, #annonces_departement', function () {
//     let $field = $(this);
//     //console.log($field);
//     let $centerField = $('#annonces_region'); // on récupère le champ caché qui contient la région
//     let $form = $field.closest('form'); // on récupère le formulaires

//     let target = '#' + $field.attr('id').replace('departement', 'ville').replace('region', 'departement');

//     let data = {};
//     data[$centerField.attr('name')] = $centerField.val();
//     data[$field.attr('name')] = $field.val();

//     $.post($form.attr('action'), data).then(function (data) {

//         let $input = $(data).find(target);
//         $(target).replaceWith($input);
//     })
// });
// // Maintenant, on ajouter un message flash pour chaque action effectuée




$(document).on('change', '#annonces_region, #annonces_departement', function () {
    let $field = $(this);
    let $centerField = $('#annonces_region'); // On récupère le champ caché qui contient la région
    let $form = $field.closest('form'); // On récupère le formulaire

    let target = '#' + $field.attr('id').replace('departement', 'ville').replace('region', 'departement');

    let data = {};
    data[$centerField.attr('name')] = $centerField.val(); // Récupère la région
    data[$field.attr('name')] = $field.val(); // Récupère le département ou la région sélectionné(e)

    // Envoi de la requête POST
    $.post($form.attr('action'), data).then(function (response) {
        let $input = $(response).find(target);
        $(target).replaceWith($input);

        // Message flash de succès
        showFlashMessage("Mise à jour réussie !");
    }).fail(function () {
        // Message flash d'erreur
        showFlashMessage("Erreur lors de la mise à jour.", true);
    });
});

// Fonction pour afficher un message flash
function showFlashMessage(message, isError = false) {
    let flashMessage = $('<div class="flash-message"></div>');
    flashMessage.text(message);

    // Ajout de la classe pour les erreurs
    if (isError) {
        flashMessage.addClass('error');
    }

    // Ajouter au corps du document
    $('body').append(flashMessage);

    // Animation
    flashMessage.css({ top: '20px', opacity: 1 });
    setTimeout(function () {
        flashMessage.css({ top: '-50px', opacity: 0 });
        setTimeout(function () {
            flashMessage.remove();
        }, 500);
    }, 3000);
}

// Styles pour les messages flash
$('<style>')
    .prop('type', 'text/css')
    .html(`
        .flash-message {
            position: fixed;
            top: -50px;
            left: 50%;
            transform: translateX(-50%);
            background: #4caf50;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
            z-index: 9999;
            opacity: 0;
            transition: all 0.5s ease;
        }

        .flash-message.error {
            background: #f44336;
        }
    `)
    .appendTo('head');
