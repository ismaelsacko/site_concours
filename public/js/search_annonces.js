document.addEventListener("DOMContentLoaded", function () {
    const searchForm = document.getElementById('search-form');
    if (searchForm) {
        searchForm.addEventListener('submit', function(e) {
            e.preventDefault();

            let query = document.getElementById('search-query').value;
            let resultsContainer = document.getElementById('search-results');

            // Afficher un message de chargement
            resultsContainer.innerHTML = '<p>Recherche en cours...</p>';
            resultsContainer.style.display = 'block';

            // Utilisation de la variable globale définie dans le template
            axios.get(window.search_annonces_url, {
                params: { query: query }
            })
            .then(function(response) {
                console.log(response.data); // Vérification dans la console
                let annonces = response.data.annonces;
                resultsContainer.innerHTML = ''; // Vider le conteneur

                if (annonces.length > 0) {
                    annonces.forEach(function(annonce) {
                        let item = document.createElement('div');
                        item.classList.add('annonce-item');
                        item.innerHTML = `
                          <h3>${annonce.title}</h3>
                          <p> Publié le ${annonce.date || 'date inconnue'}</p>
                          <img src="/uploads/${annonce.image}" alt="${annonce.title}" style="max-width:20%; height:auto; margin-bottom:10px;">
                          <p>${annonce.content}...</p>
                          <p><em> <strong>${annonce.ville}</strong> dans  le département ${annonce.departement},en région ${annonce.region}</em></p>
                          <a href="/annonce/${annonce.id}">Voir l'annonce</a>
                        `;
                        if (query == "") {
                          item.style.display = 'none';
                          resultsContainer.innerHTML = '<p class="alert alert-warning">Veuillez saisir un mot-clé (titre, description ou ville)</p>';

                        }
                        resultsContainer.appendChild(item);
                    });
                } else {
                    resultsContainer.innerHTML = '<p class="alert alert-warning">Aucune annonce trouvée</p>';
                }

                // Animation fade-in
                resultsContainer.classList.remove('fade-in');
                void resultsContainer.offsetWidth; // Forcer le reflow pour réinitialiser l'animation
                resultsContainer.classList.add('fade-in');
            })
            .catch(function(error) {
                console.error(error);
                resultsContainer.innerHTML = '<p>Une erreur est survenue</p>';
            });
        });
    }
});
