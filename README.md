# README

## Installation et utilisation de l'application

Ce projet utilise Docker pour exécuter un environnement de développement complet avec PHP, Nginx, MariaDB, Adminer et un serveur de mail.

### Prérequis

Avant de commencer, assurez-vous d'avoir installé :
- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Installation

1. Clonez ce dépôt :
   ```bash
   git clone https://gitlab.com/ismaelsacko/site_concours.git
   cd site_concours
   ```

2. Copiez le fichier `.env.example` en `.env` et adaptez les variables si nécessaire :
   ```bash
   cp .env.example .env
   ```

3. Construisez et démarrez les conteneurs :
   ```bash
   docker-compose up -d --build
   ```

4. Installez les dépendances PHP avec Composer (dans le conteneur `php`) :
   ```bash
   docker-compose exec php composer install
   ```

5. Appliquez les migrations de la base de données :
   ```bash
   docker-compose exec php php bin/console doctrine:migrations:migrate
   ```

### Accès aux services

- **Application web** : http://localhost:8000
- **Adminer (gestion base de données)** : http://localhost:8082
  - Serveur : `database`
  - Utilisateur : `root`
  - Mot de passe : `root`
  - Base de données : `app`
- **Mailpit (serveur mail de développement)** : http://localhost:8025

### Commandes utiles

- Arrêter les conteneurs :
  ```bash
  docker-compose down
  ```
- Redémarrer les conteneurs :
  ```bash
  docker-compose up -d
  ```
- Voir les logs :
  ```bash
  docker-compose logs -f
  ```
- Se connecter au conteneur PHP :
  ```bash
  docker-compose exec php bash
  ```

### Configuration Docker Compose

Voici la configuration `docker-compose.yml` utilisée :

```yaml
services:
  php:
    user: '${USER_ID}:${GROUP_ID}'
    build: ./docker/php
    volumes:
      - .:/var/www:delegated
    depends_on:
      - database
    networks:
      - app_network

  nginx:
    build: ./docker/nginx
    ports:
     - "8000:80"
    volumes:
      - ./public:/var/www/public:delegated
    depends_on:
     - php
    networks:
      - app_network           

  database:
    image: mariadb:10.7.3
    environment:
      MARIADB_USER: root
      MARIADB_ROOT_PASSWORD: root
      MARIADB_DATABASE: app
      MARIADB_ALLOW_EMPTY_ROOT_PASSWORD: 'no'
    volumes:
      - database_data:/var/lib/mysql:rw
      - ./var/mysql:/var/www/var
    networks:
      - app_network   

  adminer:
    image: adminer:latest
    depends_on:
      - database
    environment:
      APP_ENV: dev
      ADMINER_DESIGN: pepa-linha
      ADMINER_DEFAULT_SERVER:
    ports:
      - "8082:8080"
    networks:
      - app_network
      
  mailer:
    image: axllent/mailpit
    ports:
      - "1025:1025"
      - "8025:8025"
    environment:
      MP_SMTP_AUTH_ACCEPT_ANY: 1
      MP_SMTP_AUTH_ALLOW_INSECURE: 1
    networks:
      - app_network          

networks:
  app_network:

volumes:
  database_data:
```

### Notes

- Assurez-vous que les ports 8000, 8082 et 8025 ne sont pas déjà utilisés par un autre service sur votre machine.
- Pour modifier la configuration PHP, éditez le fichier `./docker/php/Dockerfile` ou les fichiers de configuration correspondants.
- Si vous rencontrez des problèmes de permissions avec les fichiers, essayez d'exécuter :
  ```bash
  sudo chown -R $USER:$USER si vous êtes sur Linux
  ```
  ou ajoutez votre utilisateur au groupe Docker.

### Ou si voulez utiliser le site en ligne directement vous pouvez le faire en cliquant sur le lien suivant : [Site en ligne](https://concours.ismael-dev.com/)
### Ou si vous voulez voir le code source du site en ligne vous pouvez le faire en cliquant sur le lien suivant : [Code source du site en ligne](https://gitlab.com/ismaelsacko/site_concours)

ET voicci les identifiants pour accéder à l'interface d'administration du site en ligne :
- **Email** : ismalsacko@yahoo.fr
- **Mot de passe** : sackosacko
Ces identifiants vous permettront de voir les candidats inscrits au concours et de les valider.

Bon développement ! 🚀

